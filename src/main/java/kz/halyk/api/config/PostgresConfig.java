package kz.halyk.api.config;

import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * Postgresql configs.
 * */

@Configuration
@ComponentScan("kz.halyk.api.config")
@EnableJpaRepositories(basePackages = "kz.halyk.api.repository")

/*
* Postgres configuration.
* */
public class PostgresConfig {
  /**
   * Data source configuration.
   * */
  @Bean
  public DataSource dataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();

    dataSource.setDriverClassName("org.postgresql.Driver");
    dataSource.setUrl("jdbc:postgresql://localhost:5434/postgres");
    dataSource.setUsername("postgres");
    dataSource.setPassword("password");

    return dataSource;
  }
}
