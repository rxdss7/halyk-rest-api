package kz.halyk.api.repository;

import java.util.List;
import kz.halyk.api.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * CrudRepository for User class.
 * */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {
  @Query(value = "SELECT * FROM users WHERE  name= :name", nativeQuery = true)
   List<User> getUserByName(@Param("name") String name);
}
