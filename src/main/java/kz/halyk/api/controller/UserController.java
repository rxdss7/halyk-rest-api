package kz.halyk.api.controller;

import io.swagger.annotations.ApiOperation;
import java.util.List;
import kz.halyk.api.model.User;
import kz.halyk.api.service.UserService;
import kz.halyk.api.util.UserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for users.
 */
@RestController
public class UserController {
  @Autowired
  UserService userService;

  @ApiOperation(value = "Get all users", notes = "Returns all users from db")
  @GetMapping("/users")
  public List<User> getUsers() {
    return userService.getUsers();
  }

  @PostMapping("/users")
  public User insertUser(@RequestBody User user) {
    return userService.save(user);
  }

  @GetMapping("/users/{name}")
  public List<User> getUserByName(@PathVariable("name") String name) throws UserException {
    return userService.getUserByName(name);
  }
}
