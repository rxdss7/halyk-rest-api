package kz.halyk.api.service;

import java.util.List;

import kz.halyk.api.model.User;
import kz.halyk.api.repository.UserRepository;
import kz.halyk.api.util.UserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service for users function.
 */
@Service
public class UserService {
  @Autowired
  public UserRepository userRepository;

  /**
   * Get all users in the system.
   */
  public List<User> getUsers() {
    return (List<User>) userRepository.findAll();
  }

  /**
   * Get insert user in system.
   */
  public User save(User user) {
    return userRepository.save(user);
  }

  /**
   * Get user by name in system.
   *
   * @param name of user.
   */
  public List<User> getUserByName(String name) throws UserException {
    // name handler: check numbers in name not allowed
    if (name.matches(".*\\d.*")) {
      return null;
    }
    if (name.split(" ").length > 4) {
      throw new UserException("long name");
    }
    return userRepository.getUserByName(name);
  }

  /**
   * Delete user in system.
   */
  public void clear(User user) {
    userRepository.delete(user);
  }
}
