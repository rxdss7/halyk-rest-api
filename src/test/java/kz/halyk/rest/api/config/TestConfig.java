package kz.halyk.rest.api.config;

import kz.halyk.api.repository.UserRepository;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class TestConfig {
  @Bean
  @Primary
  public UserRepository getUserRepository() {
    return Mockito.mock(UserRepository.class);
  }
}
