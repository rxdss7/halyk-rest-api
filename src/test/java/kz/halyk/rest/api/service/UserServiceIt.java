package kz.halyk.rest.api.service;

import java.util.List;
import kz.halyk.api.App;
import kz.halyk.api.model.User;
import kz.halyk.api.service.UserService;
import kz.halyk.api.util.UserException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
@AutoConfigureMockMvc
public class UserServiceIt {
  // TODO: add @Before and @After
  @Autowired
  UserService userService;

  /**
   * Fill database with data for tests.
   * */
  @Before
  public void fillDataInDb() {
    User user = new User();
    user.setName("student");
    user.setAge(19);
    user.setId(1);
    userService.save(user);
  }

  @Test
  public void getUserByName() throws UserException {
    String name = "student";
    List<User> users = userService.getUserByName(name);
    Assert.assertEquals(1, users.size());
  }

  /**
   * Clear database with after tests.
   * */
  @After
  public void clearDataInDb() {
    User user = new User();
    user.setName("student");
    user.setAge(19);
    user.setId(1);
    userService.clear(user);
  }
}
