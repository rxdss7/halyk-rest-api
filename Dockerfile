FROM postgres:11.6
COPY sql/1_initdb.sql /docker-entrypoint-initdb.d/
COPY sql/2_data.sql /docker-entrypoint-initdb.d/